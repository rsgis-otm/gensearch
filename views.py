# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division

import json
# import string
from django.shortcuts import render_to_response
from django.template import RequestContext

from django_tinsel.decorators import render_template, route #, json_api_call
from django_tinsel.utils import decorate as do

from treemap.decorators import instance_request
from treemap.models import Species
from treemap.views import map_feature
from treemap import routes

def render_map_feature_detail(request, instance, feature_id):
    context = map_feature.map_feature_detail(request, instance, feature_id, False)
    if context['plot']:
        context['geom_dd'] = context['plot'].geom.transform(4326, True)
        template = 'treemap/plot_detail.html'
    else:
        template = 'map_features/%s_detail.html' % feature.feature_type
    return render_to_response(template, context, RequestContext(request))

def spp_list(request, instance, format='common'):
    common = format != 'scientific'
    def get_data(spp):
        q = {}
        for field in ['genus', 'species', 'cultivar']:
            if spp[field]:
                q['species.' + field] = {'IS': spp[field]}
        sci_name = Species.get_scientific_name(spp['genus'],
                                               spp['species'],
                                               spp['cultivar'])
        return {'name': spp['common_name'] if common else sci_name,
                'other_name': sci_name if common else spp['common_name'],
                'symbol': spp['otm_code'],
                'query': json.dumps(q)}

    species_qs = instance.scope_model(Species)\
                         .order_by('common_name' if common else 'genus','species','cultivar')\
                         .values('common_name', 'genus',
                                 'species', 'cultivar', 'id', 'otm_code')
    return {'format': 'common' if common else 'scientific',
            'other_format': 'scientific' if common else 'common',
            'species': [get_data(s) for s in species_qs]}

# map_feature_view = None
# map_feature_view = do(
#     instance_request,
#     route(
#         GET=render_map_feature_detail,
#         # ELSE=None))
#         ELSE=routes.map_feature_detail))
map_feature_view = route(
    GET=do(instance_request, render_map_feature_detail),
    ELSE=routes.map_feature_detail)

species_view = do(
    instance_request,
    render_template('gensearch/species.html'),
    spp_list)


# def species_list(request, instance):
#     max_items = request.GET.get('max_items', None)

#     species_qs = instance.scope_model(Species)\
#                          .order_by('common_name')\
#                          .values('common_name', 'genus',
#                                  'species', 'cultivar', 'id')

#     if max_items:
#         species_qs = species_qs[:max_items]

#     # Split names by space so that "el" will match common_name="Delaware Elm"
#     def tokenize(species):
#         names = (species['common_name'],
#                  species['genus'],
#                  species['species'],
#                  species['cultivar'])

#         tokens = set()

#         for name in names:
#             if name:
#                 tokens = tokens.union(name.split())

#         # Names are sometimes in quotes, which should be stripped
#         return {token.strip(string.punctuation) for token in tokens}

#     def annotate_species_dict(sdict):
#         sci_name = Species.get_scientific_name(sdict['genus'],
#                                                sdict['species'],
#                                                sdict['cultivar'])

#         display_name = "%s [%s]" % (sdict['common_name'],
#                                     sci_name)

#         searches = [{'species.genus': {'LIKE': sdict['genus']}}]
#         if sdict['species']:
#             searches.append({'species.species': {'LIKE': sdict['species']}})
#         if sdict['cultivar']:
#             searches.append({'species.cultivar': {'LIKE': sdict['cultivar']}})
#         if len(searches) > 1:
#             searches.insert(0, "AND")

#         tokens = tokenize(species)

#         sdict.update({
#             'scientific_name': sci_name,
#             'value': display_name,
#             'tokens': tokens,
#             'db_id': sdict['id'],
#             'id': json.dumps(searches)})

#         return sdict

#     return [annotate_species_dict(species) for species in species_qs]

# species_list_view = do(
#     json_api_call,
#     instance_request,
#     species_list)
