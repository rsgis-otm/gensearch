# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from __future__ import division

from django.conf.urls import patterns, url
from django.contrib import admin

from treemap import routes
from treemap.instance import URL_NAME_PATTERN

import gensearch.views as views

admin.autodiscover()
instance_pattern = r'^(?P<instance_url_name>' + URL_NAME_PATTERN + r')'

urlpatterns = patterns(
    '',
    # url(instance_pattern + r'/species/$', species_list_view, name="species_list_view"),
    url(instance_pattern + r'/features/(?P<feature_id>\d+)/$', views.map_feature_view, name='map_feature_detail'),
    # url(instance_pattern + r'/features/(?P<feature_id>\d+)/$', routes.map_feature_detail, name='map_feature_detail'),
    url(instance_pattern + r'/spp(?:/(?P<format>(common)|(scientific)))?/?$', views.species_view, name="friendly_species_list"),
)
