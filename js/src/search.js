"use strict"

var $ = require('jquery'),
    BU = require('treemap/baconUtils'),
    Bacon = require('baconjs'),
    urlState = require('treemap/urlState')

exports.init = function() {
    var $input = $('#species-typeahead'),
        $genus = $('#search-species-genus'),
        $species = $('#search-species-species'),
        $cultivar = $('#search-species-cultivar'),
        selectStream = $input.asEventStream('typeahead:selected typeahead:autocompleted',
                                            function(e, datum) {
                                                return datum;
                                            }),
        backspaceOrDeleteStream = $input.asEventStream('keyup').filter(BU.keyCodeIs([8, 46])),
        genusStream = selectStream.map(".genus").merge(backspaceOrDeleteStream.map("")),
        speciesStream = selectStream.map(".species").merge(backspaceOrDeleteStream.map("")),
        cultivarStream = selectStream.map(".cultivar").merge(backspaceOrDeleteStream.map(""))

    genusStream.onValue($genus, 'val')
    speciesStream.onValue($species, 'val')
    cultivarStream.onValue($cultivar, 'val')

    // var resetStream = $("#search-reset").asEventStream("click"),
    //     triggeredQueryStream =
    //         Bacon.mergeAll(
    //             urlState.stateChangeStream // URL changed
    //                 .filter('.search')     // search changed
    //                 .map('.search'),       // get search string
    //             resetStream.map({})
    //         )

    // triggeredQueryStream.onValue(function() {
    //     console.log('triggered query')
    // })

    // urlState.init()
}
